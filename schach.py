#! /bin/python
# -*- coding: utf-8 -*-

class Feld:
    farbe = None
    figur = None
    def __init__(self, farbe):
        self.farbe = Farbe(farbe)
    def __getitem__(self):
        if self.figur == None:
            return self.farbe
        return self.figur
    def push (self, figur):
        self.figur = figur
    def pop (self):
        temp = self.figur
        self.figur = None
        return temp

class Farbe:
    name = ""
    abk = ""
    def __init__(self, name):
        self.name = name
        if name == u"weiß":
            self.abk = "1"
        else:
            self.abk = "0"

class Figur:
    abk = "?"
    def __init__ (self):
        pass
    def getBedrohungen (self):
        pass
    def setPosition (x, y):
        pass
    def __print__(self):
        return self.abk

class Bauer (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt = [[0, 1]]
        self.sprint = [[0, 2]]
        self.schlag = [[1, 1], [1, -1]]
        self.name = "Bauer"
        self.staerke = 100
        self.wert = 1
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "b"
            self.unicode = u"♙"
        else:
            self.abk = "B"
            self.unicode = u"♟"
class Pferd (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt =  [[2, 1], [2, -1], [-2, 1], [-2, -1]]
        self.schlag = self.schritt
        self.name = "Pferd"
        self.staerke = 275
        self.wert = 3
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "s"
            self.unicode = u"♘"
        else:
            self.abk = "S"
            self.unicode = u"♞"
class Laeufer (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt = [[1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], \
            [1, -1], [2, -2], [3, -3], [4, -4], [5, -5], [6, -6], [7, -7], \
            [-1, 1], [-2, 2], [-3, 3], [-4, 4], [-5, 5], [-6, 6], [-7, 7], \
            [-1, -1], [-2, -2], [-3, -3], [-4, -4], [-5, -5], [-6, -6], [-7, -7]]
        self.schlag = self.schritt
        self.name = "Läufer"
        self.staerke = 325
        self.wert = 4
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "l"
            self.unicode = u"♗"
        else:
            self.abk = "L"
            self.unicode = u"♝"
    def echoSchritt (self):
        return self.schritt
class Turm (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt = [[1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], \
            [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], \
            [-1, 0], [-2, 0], [-3, 0], [-4, 0], [-5, 0], [-6, 0], [-7, 0], \
            [0, -1], [0, -2], [0, -3], [0, -4], [0, -5], [0, -6],  [0, -7]]
        self.schlag = self.schritt
        self.name = "Turm"
        self.staerke = 465
        self.wert = 5
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "t"
            self.unicode = u"♖"
        elif farbe == "schwarz":
            self.abk = "T"
            self.unicode = u"♜"
    def echoSchritt (self):
        return self.schritt
class Dame (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt = Turm("schwarz").echoSchritt() + Laeufer("schwarz").echoSchritt()
        self.schlag = self.schritt
        self.name = "Dame"
        self.staerke = 900
        self.wert = 9
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "d"
            self.unicode = u"♕"
        else:
            self.abk = "D"
            self.unicode = u"♛"
class Koenig (Figur):
    def __init__ (self, farbe):
        Figur.__init__(self)
        self.schritt = [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]
        self.schlag = self.schritt
        self.name = "König"
        self.staerke = 200
        self.wert = 12
        self.farbe = farbe
        if farbe == u"weiß":
            self.abk = "k"
            self.unicode = u"♔"
        elif farbe == "schwarz":
            self.abk = "K"
            self.unicode = u"♚"

class armee:
    farbe = None
    einheiten = [[]]
    def __init__ (self, farbe):
        self.farbe = Farbe(farbe)
    def echo (self):
        print self.farbe
        print self.einheiten
    def aufstellen (self, welt):
        if self.farbe.name == u"weiß":
            for i in range(0, 8):
                welt.aufstellen(Bauer  (self.farbe.name), 6, i)
            welt.aufstellen(Turm   (self.farbe.name), 7, 0)
            welt.aufstellen(Pferd  (self.farbe.name), 7, 1)
            welt.aufstellen(Laeufer(self.farbe.name), 7, 2)
            welt.aufstellen(Dame   (self.farbe.name), 7, 3)
            welt.aufstellen(Koenig (self.farbe.name), 7, 4)
            welt.aufstellen(Laeufer(self.farbe.name), 7, 5)
            welt.aufstellen(Pferd  (self.farbe.name), 7, 6)
            welt.aufstellen(Turm   (self.farbe.name), 7, 7)
        else:
            for i in range(0, 8):
                welt.aufstellen(Bauer  (self.farbe.name), 1, i)
            welt.aufstellen(Turm   (self.farbe.name), 0, 0)
            welt.aufstellen(Pferd  (self.farbe.name), 0, 1)
            welt.aufstellen(Laeufer(self.farbe.name), 0, 2)
            welt.aufstellen(Dame   (self.farbe.name), 0, 3)
            welt.aufstellen(Koenig (self.farbe.name), 0, 4)
            welt.aufstellen(Laeufer(self.farbe.name), 0, 5)
            welt.aufstellen(Pferd  (self.farbe.name), 0, 6)
            welt.aufstellen(Turm   (self.farbe.name), 0, 7)
    def beiteten (self, einheit):
        self.einheiten = self.einheiten + [einheit.abk, einheit.position]

class Welt:
    brett = []
    weiss = None
    schwarz = None
    zuege = []
    def __init__ (self):
        for i in range(1, 9):
            zeile = []
            if i % 2 == 0:
                farbe = u"weiß"
            else:
                farbe = u"schwarz"
            for o in range(1, 9):
                zeile.append(Feld(farbe))
                if farbe == u"weiß":
                    farbe = u"schwarz"
                else:
                    farbe = u"weiß"
            self.brett.append(zeile)

        # Armeen aufstellen
        self.weiss = armee(u"weiß")
        self.weiss.aufstellen(self)
        self.schwarz = armee(u"schwarz")
        self.schwarz.aufstellen(self)

    def aufstellen (self, Figur, x, y):
        self.brett[x][y].push(Figur)

    # CLI-Repräsentation vom Brett ausgeben
    def echo (self):
        i = 0
        for zeile in self.brett:
            i += 1
            out = " " + str(9 - i) + " | "
            for feld in zeile:
                if not feld.figur == None:
                    out += feld.figur.abk + " "
                else:
                    out += feld.farbe.abk + " "
            print out
        print "---|----------------"
        out = "   | "
        for i in range(0, 8):
            out += chr(i + ord("a")) + " "
        print out

    # bisherige Züge ausgeben
    def printZuege(self):
        print "Die Züge der Partie:"
        print
        out = ""
        for zug in self.zuege:
            out += chr(ord("a") + zug.absolut[0][0]) + str(8 - zug.absolut[0][1]) + "," + chr(ord("a") + zug.absolut[1][0]) + str(8 - zug.absolut[1][1]) + "\n"
        print out
class Zug:
    absolut = None
    relativ = None
    spieler = None
    def __init__(self, absolut, relativ, spieler):
        self.absolut = absolut
        self.relativ = relativ

welt = Welt()

erstlauf = True

while True:
    ## Situation anzeigen ##
    welt.echo()

    ## Eingabe holen ##
    if erstlauf == True:
        eingabe = raw_input("Du fängst an. Dein Zug (xm,yn):").split(",")
        erstlauf = False
    else:
        eingabe = raw_input("Du bist dran. Dein Zug (xm,yn):").split(",")
    if eingabe[0] == "exit":
        break

    ## Eingabe formal verfizieren ##

    # Eingabe bzgl. Länge verifizieren
    if len(eingabe) != 2:
        print "Eingabe fehlerhaft. Anzahl der Positionen war " + str(len(eingabe)) + ". Erwartet wurden 2."
        continue
    startfeld = eingabe[0]
    if len(startfeld) != 2:
        print "Eingabe fehlerhaft. Länge vom Startfeld war " + str(len(startfeld)) + ". Erwartet wurde 2."
        continue
    zielfeld = eingabe[1]
    if len(zielfeld) != 2:
        print "Eingabe fehlerhaft. Länge vom Zielfeld war " + str(len(zielfeld)) + ". Erwartet wurde 2."
        continue

    # Eingabe bzgl. Typen verifizieren
    x = startfeld[:1]
    if x not in ("a","b","c","d","e","f","g","h"):
        print "Eingabe fehlerhaft. Abzissenteil des Startfelds war " + str(x) + ". Erwartet wurde ein Element von [a,b,c,d,e,f,g,h]."
        continue
    x = ord(x) - ord("a")

    try:
        m = int(startfeld[1:2])
    except ValueError:
        print "Eingabe fehlerhaft. Ordinatenteil des Startfelds war " + str(startfeld[1:2]) + ". Erwartet wurde ein Element von [1,2,3,4,5,6,7,8]."
        continue
    if m not in range(1,9):
        print "Eingabe fehlerhaft. Ordinatenteil des Startfelds war " + str(startfeld[1:2]) + ". Erwartet wurde ein Element von [1,2,3,4,5,6,7,8]."
        continue
    m = 7 - (m - 1)

    y = zielfeld[:1]
    if y not in ("a","b","c","d","e","f","g","h"):
        print "Eingabe fehlerhaft. Abzissenteil des Zielfelds war " + str(y) + ". Erwartet wurde ein Element von [a,b,c,d,e,f,g,h]."
        continue
    y = ord(y) - ord("a")

    try:
        n = int(zielfeld[1:2])
    except ValueError:
        print "Eingabe fehlerhaft. Ordinatenteil des Zielfelds war " + str(zielfeld[1:2]) + ". Erwartet wurde ein Element von [1,2,3,4,5,6,7,8]."
        continue
    if n not in range(1,9):
        print "Eingabe fehlerhaft. Ordinatenteil des Zielfelds war " + str(zielfeld[1:2]) + ". Erwartet wurde ein Element von [1,2,3,4,5,6,7,8]."
        continue
    n = 7 - (n - 1)

    ## Eingabe inhaltlich verifizieren ##

    # leeres Startfeld
    if welt.brett[m][x].figur == None:
        print "Eingabe fehlerhaft. Das Startfeld " + startfeld + " ist unbesetzt."
        continue
    figur = welt.brett[m][x].figur
    ziel = welt.brett[n][y]

    # Zugfähigkeiten normalisieren
    if figur.farbe == u"schwarz":
        schrittNr = 0
        for schritt in figur.schritt:
            figur.schritt[schrittNr][1] = -schritt[1]
            schrittNr += 1
        sprintNr = 0
        for sprint in figur.sprint:
            figur.sprint[sprintNr][1] = -sprint[1]
            sprintNr += 1

    # Aussetzen
    if x == y and m == n:
        print "Eingabe fehlerhaft. Startfeld und Zielfeld waren gleich (" + str(startfeld) + "). Aussetzen ist nicht erlaubt."
        continue

    # Friendly Fire
    try:
        if figur.farbe == ziel.figur.farbe:
            print "Eingabe fehlerhaft. Auf dem Zielfeld (" + str(zielfeld) + ") steht eine eigene Figur. Friendly Fire ist nicht erlaubt."
            continue
    except AttributeError:
        pass

    # Überspringen
    if figur.name in ["Bauer", "Turm", "Läufer", "Dame"]:
        if x - y >= 2 or y - x >= 2 or m - n >= 2 or n - m >= 2:
            print "Hmmm." #TODO!

    # Eingabe ist ein Zug.
    relativ = []
    relativ.append(x - y) # Ordinate
    relativ.append(m - n) # Abzisse
    absolut = [[x, m], [y, n]]
    spieler = welt.brett[m][x].figur.farbe
    zug = Zug(absolut, relativ, spieler)

    # Zugfähigkeit der Figur bestimmen
    if ziel.figur == None:
        schlagen = False
    else:
        schlagen = True
    # en-passant-Schlag? TOTEST TODO !!
    enPassant = None
    if not schlagen and figur.name == "Bauer":
        if zug.relativ == [-1, 1] or zug.relativ == [1, 1]:
            if welt.zuege[len(welt.zuege) - 1].absolut[0][0] == n and welt.zuege[len(welt.zuege) - 1].absolut[1][1] == m:
                schlagen = True
                enPassant = True
    else:
        enPassant = False
    print schlagen, enPassant
    if schlagen:
        if not zug.relativ in figur.schlag and not enPassant:
            print "Zug fehlerhaft. Die Einheit '" + figur.name + "' kann nicht von " + chr(ord("a") + x) + str(8 - m) + " auf " + chr(ord("a") + y) + str(8 - n) + " schlagen."
            continue
    else:
        if figur.name == "Bauer":
            if not zug.relativ in figur.sprint and not zug.relativ in figur.schritt:
                print "Zug fehlerhaft. Die Einheit '" + figur.name + "' kann nicht von " + chr(ord("a") + x) + str(8 - m) + " auf " + chr(ord("a") + y) + str(8 - n) + " setzen."
                continue
        else:
            if not zug.relativ in figur.schritt:
                print "Zug fehlerhaft. Die Einheit '" + figur.name + "' kann nicht von " + chr(ord("a") + x) + str(8 - m) + " auf " + chr(ord("a") + y) + str(8 - n) + " setzen."
                continue

    # Zugfähigkeit des Spielers bestimmen ##
    # * Ist der eigene König nach diesem Zug bedroht?
    # * Kann der Spieler überhaupt noch irgendwas setzen? Remis?
    # TODO

    ## Zug wegspeichern ##
    welt.zuege.append(zug)

    ## Bestätigung ausgeben ##
    if schlagen:
        print figur.name + " schlägt " + ziel.figur.name
    else:
        print "Du setzt von " + startfeld + " auf " + zielfeld + "."

    ## Zug ausführen ##
    figur = welt.brett[m][x].pop()
    welt.brett[n][y].push(figur)

## Züge der Partie ausgeben ##
welt.printZuege()

## Abschiedsgruß ##
print "Tschüss!"
